ZADOK
=====

A multi-language test suite runner.


1. COPYRIGHT
------------

Copyright 2004 - 2018, Simon Peter Chappell


2. PRIMARY CONTACT
------------------

The author of Zadok is Simon Chappell. Simon can be found on the web at
simonpeter.com, simonpeter.org and newlife-upc.org with a smattering of
others thrown in for good measure.

Simon's email address is simon@simonpeter.com. If that one is down, use
simonpeterchappell@gmail.com as a backup.

Simon does not enjoy writing about himself in the third person.


3. LICENCE
----------

Zadok is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Zadok is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Zadok; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


4. PURPOSE
----------

Zadok is a multi-language test suite runner. It is designed to run small
test programs written in one of the programming or scripting languages
that it understands, compare their output to what is expected and
determine a pass or fail. Tests are grouped into suites for conceptual
organization purposes.


5. USAGE
--------

To run Zadok, place "zadok.sh" and "zdk_*.sh" on the PATH. The traditional way
to do this, on Linux and Mac OS X, is to add a snippet like the following one
to your .bash_profile file.

~~~
  ZADOK_HOME=/opt/zadok
  export ZADOK_HOME
  PATH=$PATH:$ZADOK_HOME/bin
~~~

From the directory where Zadok is started, Zadok will look for any
directory that ends with the text "Suite" and will consider it to be a
suite of test programs. Zadok will then look inside each suite to check
for the presence of a file called "suitename.txt" and directories with
names that end in "Test". Zadok will then execute and test the output of
each program that it recognizes and it has the resources to run.


6. DESIGN
---------

The design of Zadok, while no means perfect, is designed to overcome the
problem of testing suites of programs written in multiple programming
languages. When working with a single programming language, there are
often testing libraries or utilities specifically for that language, for
example JUnit for the Java language. Unfortunately, these libraries are
generally unsuitable for multiple languages.

To test programs written in multiple programming languages, it is
necessary to find the lowest common denominator, or common factor,
between programs. The common factors that fit the requirements are the
standard input, standard output and command line parameters. At this
time Zadok uses the standard output, which allows it to  receive
consistent output from the programs under test. Being able to get a copy
of the output from the program enables us to determine whether it was
what we expected and therefore correct.

Zadok uses a plugin capability for the language runners. Previously,
each known language had to be added to the body of the Zadok code
specifically. With plugins, each language suffix found is compared to
the available runner plugins and where a match is found, the runner is
used, otherwise that language is skipped.


7. PROGRAMMING LANGUAGES
------------------------

At this time Zadok supports programs written in the following
programming and scripting languages (numbers in parenthesis are the
versions used by the author at this time):

Bash (4.4.19), Groovy (2.4.15), C (gcc 7.3.0), Clisp (2.49.60),
Erlang/OTP (9.3), Java (1.8.0_171), Ruby (2.5.1p57), Lua (5.2.4), Perl (5),
Python (2.7.15rc1), AWK (4.1.4), Haskell (GHC 8.0.2), OCaml (4.05.0),
REXX (Regina 3.6), Scala (2.11.12.final) and TCL (8.6).

Wherever possible, the languages are installed from the Linux package
repository. The author uses Ubuntu Linux, so a number of languages are
already installed as part of the distribution.


8. TO DO
--------

There are many functionalities that could be added:

*  More test suites. (under active development at this time)
*  More programming and scripting languages.
*  Run only a specified suite.
*  Run only tests written in a specified language.
*  Save the results to a file in either plain text or XML format.
*  It would be interesting to port Zadok to other languages.


9. THANKS
---------

Many thanks are due to my friend Mike Clark for his encouragement in all
matters of testing. William Juroff has given so much help and design
advice that he also deserves an honorable mention.
