#!/usr/bin/env tclsh

#
# Extract the passed parameters.
set the_program [lindex $argv 0]
set expected_output [lindex $argv 1]
set filename [lindex $argv 2]
set extension [lindex $argv 3]
set argument_list [lindex $argv 4]
set expected_return_value [lindex $argv 5]

#
# Where are we running this test?
set pwd $::env(PWD)
set absolute_path [file join $pwd $the_program]
set abs_dir [file dirname $absolute_path]

#
# Check that PHP is installed.
set code [catch {exec which php 2> /dev/null} n]
if {$code == 0} {
  #
  # We found PHP, so switch to the directory holding the test.
  cd $abs_dir
  #
  # Run the PHP test.
  set code [catch {exec php ${filename}.${extension} ${argument_list}} actual_output]
  #
  # Capture the test exit code.
  set retVal 0
  if {$code != 0} {
    set retVal [lindex $::errorCode 2]
  }
  #
  # If we received a specified expected return value, test for that,
  # otherwise only test the actual output against the expected output.
  if {[string length $expected_return_value] > 0} {
    #
    # Test for the expected return value.
    if {$retVal == $expected_return_value} {
      set the_result "PASS"
    } else {
      set the_result "FAIL"
    }
  } else {
    #
    # Test for the expected output.
    if {[string equal $expected_output $actual_output]} {
      set the_result "PASS"
    } else {
      set the_result "FAIL"
    }
  }
  #
  # Share our test result with the world.
  set formatStr {%-17s%-15s%15s}
  puts [format $formatStr "    PHP:" ${filename}.${extension} ${the_result}]
}
