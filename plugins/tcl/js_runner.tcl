#!/usr/bin/env tclsh

set the_program [lindex $argv 0]
set expected_output [lindex $argv 1]
set filename [lindex $argv 2]
set extension [lindex $argv 3]

# Check for Node.JS.
set code [catch {exec which node 2> /dev/null} n]
if {$code == 0} {
  set actual_output [exec node $the_program]
  if {[string equal $expected_output $actual_output]} {
    puts "    JavaScript:  ${filename}.${extension} OK"
  } else {
    puts "    JavaScript:  ${filename}.${extension} FAIL"
  }
}
