#!/bin/bash

assert_equals () {
  expected=$1
  received=$2
  if [ "${expected}" = "${received}" ]; then
    echo -n " PASS"
  else
    echo " FAIL, received ${received} expected ${expected}"
  fi
}

theProgram=$1
expectedOutput=$2
commandLineArgs=$3
expectedReturnValue=$4
if [ `which lua 2> /dev/null` ]; then
    printf "    Lua:         %s " ${theProgram}
    actualOutput=`lua ${theProgram} ${commandLineArgs}`
    actualReturnValue=$?
    assert_equals "${expectedOutput}" "${actualOutput}"
    # Optional tests.
    if [ -n "${expectedReturnValue}" ]; then
      assert_equals "${expectedReturnValue}" "${actualReturnValue}"
    fi
    echo ""
fi
