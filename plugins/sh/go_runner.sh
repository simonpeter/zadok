#!/bin/bash

assert_equals () {
  expected=$1
  received=$2
  if [ "${expected}" = "${received}" ]; then
    echo -n " PASS"
  else
    echo " FAIL, received ${received} expected ${expected}"
  fi
}

theProgram=$1
expectedOutput=$2
commandLineArgs=$3
expectedReturnValue=$4
prefix=${theProgram%.*}
suffix=${theProgram##*.}
if [ `which go 2> /dev/null` ]; then
    printf "    Go:          %s " ${theProgram}
    go build ${theProgram}
    actualOutput=`./${prefix} ${commandLineArgs}`
    actualReturnValue=$?
    assert_equals "${expectedOutput}" "${actualOutput}"
    # Optional tests.
    if [ -n "${expectedReturnValue}" ]; then
      assert_equals "${expectedReturnValue}" "${actualReturnValue}"
    fi
    echo ""
    # Clean up
    rm ${prefix}
fi
