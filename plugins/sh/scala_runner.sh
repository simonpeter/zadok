#!/bin/bash

assert_equals () {
  expected=$1
  received=$2
  if [ "${expected}" = "${received}" ]; then
      echo "ok"
  else
      echo "failed test, received ${received} expected ${expected}"
  fi
}

theProgram=$1
expectedOutput=$2
commandLineArgs=$3
prefix=${theProgram%.*}
suffix=${theProgram##*.}

if [ `which scala 2> /dev/null` ]; then
  echo "    Scala:       ${theProgram} DISABLED"
  # printf "    Scala:       %s " ${theProgram}
  # scalac ${theProgram}
  # actualOutput=`scala ${prefix} ${commandLineArgs}`
  # assert_equals "${expectedOutput}" "${actualOutput}"
  # rm ${prefix}.class
  # rm ${prefix}$.class
fi
