<?php
$minusNFlag = false;
$argCount = $argc;
$firstArg = 1;

if ($argc == 1) {
  echo "\n";
} else {
  if ($argv[1] == "-n") {
    $minusNFlag = true;
    $firstArg = 2;
  }
  for ($i = $firstArg; $i < $argc; $i++) {
    if ($i > $firstArg) { echo " "; }
    echo $argv[$i];
  }
  if (!$minusNFlag) {
    echo "\n";
  }
}
?>
