#!/bin/bash
#
# zdk_detect_languages.sh
#
# Copyright (c) 2018 Simon Peter Chappell
#
# See README.txt and CHANGELOG.txt for more information.
#

# This file is part of Zadok.
#
# Zadok is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Zadok is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Zadok; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

echo "Languages found: ${languagesFound}"
echo "    Shell ${BASH_VERSION} "

    groovy="no"
    c="no"
    java="no"
    erlang="no"
    ruby="no"
    lua="no"
    perl="no"
    python="no"
    awk="no"
    haskell="no"
    rexx="no"
    clisp="no"
    scala="no"
    ocaml="no"

#
# Now see which languages we have
#
    groovyTest=`which groovy 2> /dev/null`
    if [ $? == 0 ]; then
        groovy="yes"
        groovyVersion=`groovy -v 2>&1 | head -1 | awk '{print $3}'`
        echo "    Groovy ${groovyVersion}"
    else
        echo "    Groovy not found"
    fi

    ccTest=`which cc 2> /dev/null`
    if [ $? == 0 ]; then
        c="yes"
        ccVersion=`cc -v 2>&1 | grep -i "gcc version" | awk '{print $3}'`
        echo "    C ${ccVersion}"
    else
        echo "    C not found"
    fi

    javaTest=`which java 2> /dev/null`
    if [ $? == 0 ]; then
        java="yes"
        javaVersion=`java -version 2>&1 | grep -i version | cut -f 3 -d " " | cut -f 2 -d "\""`
        echo "    Java ${javaVersion}"
    else
        echo "    Java not found"
    fi

    erlangTest=`which erl 2> /dev/null`
    if [ $? == 0 ]; then
        erlang="yes"
        erlVersion=`erl -version 2>&1 | cut -f 6 -d " "`
        echo "    Erlang ${erlVersion}"
    else
        echo "    Erlang not found"
    fi

    rubyTest=`which ruby 2> /dev/null`
    if [ $? == 0 ]; then
        ruby="yes"
        rubyVersion=`ruby -v 2>&1 | cut -f 2 -d " "`
        echo "    Ruby ${rubyVersion}"
    else
        echo "    Ruby not found"
    fi

    luaTest=`which lua 2> /dev/null`
    if [ $? == 0 ]; then
        lua="yes"
        luaVersion=`lua -v 2>&1 | cut -f 2 -d " "`
        echo "    Lua ${luaVersion}"
    else
        echo "    Lua not found"
    fi

    perlTest=`which perl 2> /dev/null`
    if [ $? == 0 ]; then
        perl="yes"
        perlVersion=`perl -v 2>&1 | grep -i "this is perl" | cut -f 4 -d " "`
        echo "    Perl ${perlVersion}"
    else
        echo "    Perl not found"
    fi

    pythonTest=`which python 2> /dev/null`
    if [ $? == 0 ]; then
        python="yes"
        pythonVersion=`python -V 2>&1 | cut -f 2 -d " "`
        echo "    Python ${pythonVersion}"
    else
        echo "    Python not found"
    fi

    awkTest=`which awk 2> /dev/null`
    if [ $? == 0 ]; then
        awk="yes"
        awkVersion=`awk --version 2>&1 | head -1 | cut -f 3 -d " "`
        echo "    AWK ${awkVersion}"
    else
        echo "    AWK not found"
    fi

    haskellTest=`which ghc 2> /dev/null`
    if [ $? == 0 ]; then
        haskell="yes"
        haskellVersion=`ghc -v 2>&1 | head -1 | cut -f 5 -d " " | cut -f 1 -d ","`
        echo "    Haskell ${haskellVersion}"
    else
        echo "    Haskell not found"
    fi

    rexxTest=`which rexx 2> /dev/null`
    if [ $? == 0 ]; then
        rexx="yes"
        rexxVersion=`rexx -v 2>&1 | cut -f 1 -d " "`
        echo "    REXX ${rexxVersion}"
    else
        echo "    Rexx not found"
    fi

    clispTest=`which clisp 2> /dev/null`
    if [ $? == 0 ]; then
        clisp="yes"
        clispVersion=`clisp --version | head -1 | cut -f 3 -d " "`
        echo "    CLisp ${clispVersion}"
    else
        echo "    CLisp not found"
    fi

    scalaTest=`which scala 2> /dev/null`
    if [ $? == 0 ]; then
        scala="yes"
        scalaVersion=`scala -version 2>&1 |  cut -f 5 -d " "`
        echo "    Scala ${scalaVersion}"
    else
        echo "    Scala not found"
    fi

    ocamlTest=`which ocaml 2> /dev/null`
    if [ $? == 0 ]; then
        ocaml="yes"
        ocamlVersion=`ocaml -vnum 2>&1 | cut -f 1 -d " "`
        echo "    OCaml ${ocamlVersion}"
    else
        echo "    OCaml not found"
    fi
