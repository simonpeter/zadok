#!/usr/bin/env tclsh

#
# zadok.tcl
#
# Copyright (c) 2004 - 2018 Simon Peter Chappell
#
# Inspired by Mike Clark.
# Helped by William C. Juroff Jr.
#
# Started:  3 December 2004
# Modified: 22 September 2018
#
# Contact the author: simon@simonpeter.com
#

# This file is part of Zadok.
#
# Zadok is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Zadok is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Zadok; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#
# Functions start here.
#
proc getFileContentsAsString {filename} {
  if {[file exists ${filename}]} {
    set f [open $filename]
    set s [read $f]
    close $f
    set return_value [string trimright $s "\n"]
    return $return_value
  } else {
    return ""
  }
}

proc run_tests {test_folder test_name} {
  puts "  Found $test_name Test Folder"
  set expected_output [getFileContentsAsString "${test_folder}/expected.txt"]
  set argument_list [getFileContentsAsString "${test_folder}/arguments.txt"]
  set expected_return_value [getFileContentsAsString "${test_folder}/returnvalue.txt"]
  set list_of_test_programs [lsort [glob -nocomplain -directory "$test_folder" -type f *]]
  foreach test $list_of_test_programs {
    set raw_extension [file extension $test]
    if {$raw_extension != ".txt"} {
      set filename [file rootname [file tail $test]]
      set raw_extension_length [string length $raw_extension]
      set extension [string range $raw_extension 1 $raw_extension_length]
      run_individual_test $test $filename $extension $expected_output $argument_list $expected_return_value
    }
  }
}

proc run_individual_test {test filename extension expected_output argument_list expected_return_value} {
  global zadok_home
  set runner "${zadok_home}/plugins/tcl/${extension}_runner.tcl"
  # Check to see if runner exists.
  if {[file exists $runner]} {
    # Now call the runner with the program and the expected output.
    set results_output [exec tclsh $runner $test $expected_output $filename $extension $argument_list $expected_return_value]
    if {[string length $results_output] > 0} {
      puts "$results_output"
    }
  }
}


#
# Main program starts here.
#


#
# Display program banner.
#
puts "Zadok Test Suites Runner"
puts "(c) Simon P. Chappell, 2004-2018"
puts "version 0.5.0"
puts "This software is, and all of the accompanying test suites are, free software licenced under the GNU General Public Licence (GPL), version 2 or any later version. Read COPYING.txt for the full licence."
puts ""


#
# Read any required environment variables.
#
set zadok_home $::env(ZADOK_HOME)


#
# Search, starting from the current directory, for any directories whose
# names end in "Suite".
#
set list_of_folders [lsort [glob -nocomplain -directory "." -type d *Suite]]
foreach folder $list_of_folders {
    # Search for file called "suitename.txt" inside the suite.
    # If it doesn't exist, just call the Suite the same as its directory.
    if {[file exists "$folder/suitename.txt"]} {
        set the_suite_name [getFileContentsAsString "$folder/suitename.txt"]
    } else {
        set the_suite_name $folder
    }
    puts "Found $the_suite_name Suite"

    # Now look for directories, within the Suite, whose names end in Test.
    set list_of_test_folders [lsort [glob -nocomplain -directory "$folder" -type d *Test]]
    foreach test_folder $list_of_test_folders {
        # Search for file called "testname.txt" inside the test.
        # If it doesn't exist, just call the test the same as its directory.
        if {[file exists "$test_folder/testname.txt"]} {
            set the_test_name [getFileContentsAsString "$test_folder/testname.txt"]
        } else {
            set the_test_name $test_folder
        }

        # Search for file called "expected.txt" inside the test.
        # The directory cannot be considered a test without this file.
        if {[file exists "$test_folder/expected.txt"]} {
            run_tests $test_folder $the_test_name
        }

    }
}
