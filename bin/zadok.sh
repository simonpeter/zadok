#!/bin/bash
#
# zadok
#
# Copyright (c) 2004 - 2018 Simon Peter Chappell
#
# Inspired by Mike Clark.
# Helped by William C. Juroff Jr.
#
# Started:  3 December 2004
# Modified: 4 September 2018
#
# Contact the author: simon@simonpeter.com
#                 or: simonpeterchappell@gmail.com
#

# This file is part of Zadok.
#
# Zadok is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Zadok is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Zadok; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


function run_tests() {
  #
  # Loop through each of the example programs.
  # (everything that isn't a text file)
  #
  for exampleProgram in $(ls | grep -v .txt 2> /dev/null)
  do
      #
      # Determine the type of program that we've found.
      #
      prefix=${exampleProgram%.*}
      suffix=${exampleProgram##*.}

      #
      # Get the expected output text.
      #
      if [ -e "${prefix}.expected.txt" ]; then
          expectedOutput=`cat ${prefix}.expected.txt`
      else
          expectedOutput=`cat expected.txt`
      fi

      #
      # Get any command-line arguments. These are optional.
      #
      commandLineArgs=""
      if [ -e "${prefix}.arguments.txt" ]; then
          commandLineArgs=`cat ${prefix}.arguments.txt`
      else
          if [ -e "arguments.txt" ]; then
              commandLineArgs=`cat arguments.txt`
          fi
      fi

      #
      # Get any expected return values. These are optional.
      #
      returnValues=""
      if [ -e "${prefix}.returnvalue.txt" ]; then
          returnValues=`cat ${prefix}.returnvalue.txt`
      else
          if [ -e "returnvalue.txt" ]; then
              returnValues=`cat returnvalue.txt`
          fi
      fi

      #
      # Now Compile/Run/Clean as appropriate.
      #
      runner=${ZADOK_HOME}/plugins/sh/${suffix}_runner.sh
      if [ -e "${runner}" ]; then
          sh ${runner} "${exampleProgram}" "${expectedOutput}" "${commandLineArgs}" "${returnValues}"
      fi
  done
}


function execute_tests() {
  #
  # Loop through each of the test directories.
  #
  ls -d1 *Test |
      while read theTest
      do
          #
          # Check that the entry is a directory.
          #
          if [ -d "${theTest}" ]; then
              #
              # Test for expected files within test directory.
              #
              if [ -e "${theTest}/expected.txt" -a -e "${theTest}/testname.txt" ]; then
                  #
                  # Change to the directory and announce the test name.
                  #
                  #echo "DBG About to run tests in ${theTest}"
                  pushd "${theTest}" > /dev/null 2>&1
                  echo "  Test: `cat testname.txt`"
                  run_tests
                  popd > /dev/null 2>&1
              else
                  echo "Did not find expected.txt or testname.txt in ${theTest}!"
              fi
          else
              echo "${theTest} is not a directory!"
          fi
      done
}


function execute_suites() {
  ls -d1 *Suite |
  while read theSuite
      do
      #
      # Check that the entry is a directory.
      #
      if [ -d "${theSuite}" ]; then
          #
          # Test for expected files within suite directory.
          #
          if [ -e "${theSuite}/suitename.txt" ]; then
              #
              # Change to the directory and announce the test name.
              #
              pushd "${theSuite}" > /dev/null 2>&1
              echo
              echo Executing tests in `cat suitename.txt` Suite
              execute_tests
              popd > /dev/null 2>&1
          else
              echo Cannot find file suitename.txt in ${theSuite}!
          fi
      else
          echo ${theSuite} is not a directory!
      fi
  done
}


#
# The main driving code.
#

echo "Zadok Test Suites Runner"
echo "(c) Simon P. Chappell, 2004-2018"
echo "version 0.5.0"
echo "This software is, and all of the accompanying test suites are, free software licenced under the GNU General Public Licence (GPL), version 2 or any later version. Read COPYING.txt for the full licence."
echo

execute_suites

echo
echo "Finished"
echo

exit 0
