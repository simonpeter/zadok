/*
 * Simple.java
 */

// Zadok - A multi-language test suite runner.
// Copyright (C) 2004-2012  Simon P. Chappell
//
// Zadok is free software licenced under the GNU General Public Licence (GPL),
// version 2 or any later version. Read COPYING.txt for the full licence.

/**
 * @author  Simon P. Chappell
 * @version 8th February 2012
 */
public class Simple {
    public static void main(String[] argv) {
        System.out.println("Ready to rock and roll!");
    }
}
